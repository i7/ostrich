import typing
import dataclasses
import formula
import mona
import enum

Topology = enum.Enum("Topology", "RING HEADEDRING CROWD ARRAY")


broadcast_variable = formula.Variable("b")
broadcast_variable_snd = formula.Variable("c")
broadcast_variable_thrd = formula.Variable("d")

def rendezvous_variables(how_many: int) -> typing.Tuple[formula.Variable, ...]:
    return tuple([formula.Variable(f"r{i}") for i in range(how_many)])


@dataclasses.dataclass(frozen=True)
class StateChange:
    preset: typing.Tuple[str, ...]
    postset: typing.Tuple[str, ...]


@dataclasses.dataclass(frozen=True)
class Broadcast:
    guard: formula.Formula
    changes: typing.Tuple[StateChange, ...]


@dataclasses.dataclass(frozen=True)
class Rendezvous:
    guard: formula.Formula
    changes: typing.Tuple[typing.Tuple[formula.Term,
                                       typing.Tuple[StateChange, ...]]]

    def __post_init__(self):
        _changes = {t: state_changes for t, state_changes in self.changes}
        object.__setattr__(self, "_changes", _changes)

        all_vars = ({v for v in self.changing_terms
                       if isinstance(v, formula.Variable)}
                    | self.guard.variables)
        expected_vars = rendezvous_variables(len(all_vars))
        unexpected_vars = all_vars - frozenset(expected_vars)
        if unexpected_vars:
            raise ValueError(f"Unexpected variables {unexpected_vars} in "
                             f"rendezvous {self}")

        object.__setattr__(self, "_quantified_vars", expected_vars)

    @property
    def quantified_vars(self) -> typing.Tuple[formula.Variable, ...]:
        return self._quantified_vars  # type: ignore

    @property
    def changing_terms(self) -> typing.FrozenSet[formula.Term]:
        return frozenset(self._changes.keys())  # type: ignore

    def get_changes(self, key: formula.Term) -> typing.Tuple[StateChange, ...]:
        """ Returns the changes for `key` """
        return self._changes[key]  # type: ignore


@dataclasses.dataclass(frozen=True)
class Transition:
    rendezvous: Rendezvous
    broadcasts: typing.Tuple[Broadcast, ...]

    def __post_init__(self):
        expected_vars = (frozenset(self.rendezvous.quantified_vars)
                         | {broadcast_variable})
        for broadcast in self.broadcasts:
            unexpected_vars = broadcast.guard.variables - expected_vars
            if unexpected_vars:
                raise ValueError(f"Unexpected variables {unexpected_vars} in "
                                 f"broadcast {broadcast}")


@dataclasses.dataclass(frozen=True)
class System:
    states: typing.Tuple[str, ...]
    cover: typing.Tuple[typing.Tuple[str, ...], ...]
    default_initial_states: typing.Tuple[str, ...]
    special_initial_states: typing.Tuple[typing.Tuple[formula.Constant,
                                                      typing.Tuple[str, ...]],
                                         ...]
    topology: typing.Optional[Topology]
    transitions: typing.Tuple[Transition, ...]
    properties: typing.Tuple[typing.Tuple[str, str], ...]
    skip: typing.Tuple[str, ...]
    minimal_size: int

    def __post_init__(self):
        covered_states = set()
        for cover_element in self.cover:
            element = set(cover_element)
            if double := covered_states & element:
                raise ValueError(f"State(s) {double} are not uniquely covered")
            else:
                covered_states |= element
        uncovered_states = frozenset(set(self.states) - covered_states)
        if uncovered_states:
            raise ValueError(f"State(s) {uncovered_states} are not covered")
        unknown_states = ((frozenset(self.default_initial_states)
                           & frozenset(covered_states))
                          - frozenset(self.states))
        if unknown_states:
            raise ValueError(f"Unknown default initial states: "
                             f"{unknown_states}")
        for constant, initial_states in self.special_initial_states:
            unknown_states = (frozenset(initial_states)
                              - frozenset(self.states))
            if unknown_states:
                raise ValueError(f"Unknown initial states for {constant}: "
                                 f"{unknown_states}")
        object.__setattr__(self,
                           "_properties",
                           {k: v for k, v in self.properties})

        # check transition consistency
        for t in self.transitions:
            res = mona.check_formula(
                    mona.env.get_template(
                        "transition-consistency.mona").render(
                            system=self,
                            transition=t,
                            broadcast_variable=broadcast_variable))
            if "Formula is unsatisfiable" not in res:
                model = mona.get_model(res)
                raise ValueError(f"Inconsistent transition {t} by model "
                                 f"{model}")


    def initialfy_state(self, state) -> str:
        if not state in self.states:
            raise ValueError(f"Cannot initialfy non-existent state {state}")
        return f"initial{state}"

    def trapify_state(self, state) -> str:
        if not state in self.states:
            raise ValueError(f"Cannot trapify non-existent state {state}")
        return f"T{state}"

    def siphonify_state(self, state) -> str:
        if not state in self.states:
            raise ValueError(f"Cannot siphonify non-existent state {state}")
        return f"S{state}"

    def flowify_state(self, state) -> str:
        if not state in self.states:
            raise ValueError(f"Cannot flowify non-existent state {state}")
        return f"F{state}"

    def get_property(self, name) -> str:
        return self._properties[name]  # type: ignore

    @property
    def property_names(self) -> typing.Tuple[str, ...]:
        return tuple(name for name, _ in self.properties)

    @property
    def trap_states(self) -> typing.Tuple[str, ...]:
        return tuple(self.trapify_state(s) for s in self.states)

    @property
    def siphon_states(self) -> typing.Tuple[str, ...]:
        return tuple(self.siphonify_state(s) for s in self.states)

    @property
    def flow_states(self) -> typing.Tuple[str, ...]:
        return tuple(self.flowify_state(s) for s in self.states)
