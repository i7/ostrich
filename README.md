# ostrich

`ostrich` is a tool to analyse families of Petri nets. More specifically,
`ostrich` analyses parameterized Petri nets. Here parameterized Petri nets are
specifications which can be instantiated to concrete Petri nets by fixing any
positive value for the parameter `n` of participating agents. Consider, for
example, the famous problem of the [dining
philosophers](https://en.wikipedia.org/wiki/Dining_philosophers_problem).
Famously, the dining philosophers are able to deadlock and, consequently, then
starve to death. This can be circumvent by introducing a philosopher who grabs
her forks the other way around than all the others.

## Parameterized Petri nets

Conceptually, we consider parameterized Petri nets specified by a set of
places, `P` and two
[WS1S](https://en.wikipedia.org/wiki/Monadic_second-order_logic)-formulae `Tr`
and `Init`. Roughly speaking fixing a value for `n` corresponds to a Petri net
with places `P x [n]`; i.e. `n` copies of each place in `P` -- indexed by `0`
to `n-1`. And transitions are tuple `(P, Q)` such that the formula `Tr(n, P,
Q)` is satisifed. Moreover, we say a marking `M` is an initial marking of an
instances if the formula `Init(n, M)` is satisfied. This leaves us with a very
rich specification language; however, it turns out that a lot of interesting
examples can be described by a restricted language.

## Specifying parameterized Petri nets

We start with the [description](examples/rings/left-hander.json) of the
aforementioned dining philosophers:
```
{
  "name": "Dining philosopher with left hander",
  "info": "A version of the dining philosopher problem where one philosopher grabs their forks the other way around than everyone else ensuring a non-deadlocking protocol.",
  "states": ["waiting", "hungry", "eating", "free", "busy"],
  "cover": [["waiting", "hungry", "eating"],
            ["free", "busy"]],
  "topology": "headed ring",
  "initial": {
    "others": ["waiting", "free"]
  },
  "transitions": [
    {
      "rendezvous": {
        "guard": "0 < p",
        "p": [
          {
            "preset": ["waiting", "free"],
            "postset": ["hungry", "busy"]
          }
        ]
      }
    }, {
      "rendezvous": {
        "guard": "0 < p & p >> q",
        "p": [
          {
            "preset": ["hungry"],
            "postset": ["eating"]
          }
        ],
        "q": [
          {
            "preset": ["free"],
            "postset": ["busy"]
          }
        ]
      }
    }, {
      "rendezvous": {
        "0": [
          {
            "preset": ["waiting"],
            "postset": ["hungry"]
          }
        ],
        "1": [
            {
            "preset": ["free"],
            "postset": ["busy"]
          }
        ]
      }
    }, {
      "rendezvous": {
        "0": [
          {
            "preset": ["hungry", "free"],
            "postset": ["eating", "busy"]
          }
        ]
      }
    }, {
      "rendezvous": {
        "guard": "p >> q",
        "p": [
            {
            "preset": ["eating", "busy"],
            "postset": ["waiting", "free"]
          }
        ],
        "q": [
            {
            "preset": ["busy"],
            "postset": ["free"]
          }
        ]
      }
    }
  ]
}
```

Our description language is based on [JSON](https://www.json.org/json-en.html)
and requires us to specify the values `"states"`, `"cover"`, `"initial"`, and
`"transitions"`. We restrict ourselves to 1-bounded Petri nets; i.e., Petri
nets where every place is at every moment in time marked by exactly one place.
To ensure this, `"cover"` specifies a partition of `"states"` such that every
set in `"cover"` for every index `i` holds initially exactly one token and at
every subsequent moment maintains this property. This can be checked using the
decidability of WS1S (in form of [MONA](https://www.brics.dk/mona/)).

### Transitions

Transitions are specified by some agents rendezvousing and, additionally,
broadcasting this rendezvous to the remaining agents. The rendezvousing can be
specified with a guard which allows for linear comparisons: `<, <=, >, >=, >>,
<<` where `<<` means predecessor (modulo `n`) and `>>` successor (modulo `n`).

## Analysis

`ostrich` allows for a fully automatic analysis of the provided Petri nets and
an interactive analysis. The fully automatic analysis relies on proving finite
instances of the specified parameterized Petri net using P-Invariants.
P-Invariants are commonly used analysis tools for Petri nets and describe for
example sets of places which can never be emptied. Using sound abstraction
techniques we derive from concrete P-Invariants families of P-Invariants in
form of WS1S-formulae which then instantiate just like parameterized Petri nets
to P-Invariants for any instance. With this we can establish various safety
properties of parameterized Petri nets fully automatically; and even obtain
small sets of formulae which explain the obtained results.

In contrast to that the interactive mode (invoked using the option
`--interactive``) provides a REPL which allows the user to dismiss potential
counterexamples via P-Invariants like traps, or 1-flows. The user can then ask
the system to generalize these P-Invariants for them or provide an inductive
invariant of the system. This invariant is then checked for inductiveness and
-- if validated -- used to refine the considered abstraction of the system.

## Dependencies

The required dependencies should be readily available using `pip`. Note that
`ostrich` makes heavy use of `mona` and, consequently, requires `mona` to be in
the path. For convenience we provided a Dockerfile which builds an environment
in which `ostrich` can be used.

## TACAS 2020

A previous version of `ostrich` was submitted to
[TACAS'20](https://link.springer.com/chapter/10.1007%2F978-3-030-45190-5_13).
Since its submission we fixed a crucial bug. Please refer to the branch
`tacas-20-bugfix` to find the latest implementation of this approach.
