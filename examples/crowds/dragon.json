{
  "name": "Dragon cache coherence protocol",
  "info": "This description is lifted from https://person.dibris.unige.it/delzanno-giorgio/protocol.html",
  "states": ["invalid", "dirty", "exclusive", "shared", "dirtyshared"],
  "cover": [["invalid", "dirty", "exclusive", "shared", "dirtyshared"]],
  "initial": {
    "0": ["dirty"],
    "others": ["invalid"]
  },
  "topology": "crowd",
  "properties": {
    "exclusivedirty": "ex1 i, j: 0 <= i & i < n & 0 <= j & j < n & i ~= j & i in dirty & j in exclusive",
    "shareddirty": "ex1 i, j: 0 <= i & i < n & 0 <= j & j < n & i ~= j & i in dirty & j in shared",
    "dirtyshareddirty": "ex1 i, j: 0 <= i & i < n & 0 <= j & j < n & i ~= j & i in dirty & j in dirtyshared",

    "exclusiveshared": "ex1 i, j: 0 <= i & i < n & 0 <= j & j < n & i ~= j & i in exclusive & j in shared",
    "dirtysharedexclusive": "ex1 i, j: 0 <= i & i < n & 0 <= j & j < n & i ~= j & i in dirtyshared & j in exclusive",

    "dirtydirty": "ex1 i, j: 0 <= i & i < n & 0 <= j & j < n & i ~= j & i in dirty & j in dirty" ,

    "exclusiveexclusive": "ex1 i, j: 0 <= i & i < n & 0 <= j & j < n & i ~= j & i in exclusive & j in exclusive"
  },
  "transitions": [
    {
      "rendezvous": {
        "p": [
          {
            "preset": ["invalid"],
            "postset": ["exclusive"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "guard": "p ~= q",
        "p": [
          {
            "preset": ["invalid"],
            "postset": ["shared"]
          }
        ],
        "q": [
          {
            "preset": ["exclusive"],
            "postset": ["shared"]
          }, {
            "preset": ["dirty"],
            "postset": ["dirtyshared"]
          }, {
            "preset": ["dirtyshared"],
            "postset": ["dirtyshared"]
          }, {
            "preset": ["shared"],
            "postset": ["shared"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p & o ~= q",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }, {
              "preset": ["exclusive"],
              "postset": ["shared"]
            }, {
              "preset": ["dirty"],
              "postset": ["dirtyshared"]
            }, {
              "preset": ["shared"],
              "postset": ["shared"]
            }, {
              "preset": ["dirtyshared"],
              "postset": ["dirtyshared"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "p": [
          {
            "preset": ["exclusive"],
            "postset": ["exclusive"]
          }, {
            "preset": ["dirty"],
            "postset": ["dirty"]
          }, {
            "preset": ["shared"],
            "postset": ["shared"]
          }, {
            "preset": ["dirtyshared"],
            "postset": ["dirtyshared"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }, {
              "preset": ["exclusive"],
              "postset": ["shared"]
            }, {
              "preset": ["dirty"],
              "postset": ["dirtyshared"]
            }, {
              "preset": ["shared"],
              "postset": ["shared"]
            }, {
              "preset": ["dirtyshared"],
              "postset": ["dirtyshared"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "p": [
          {
            "preset": ["invalid"],
            "postset": ["dirty"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "guard": "p ~= q",
        "p": [
          {
            "preset": ["invalid"],
            "postset": ["dirtyshared"]
          }
        ],
        "q": [
          {
            "preset": ["exclusive"],
            "postset": ["shared"]
          }, {
            "preset": ["dirty"],
            "postset": ["shared"]
          }, {
            "preset": ["dirtyshared"],
            "postset": ["shared"]
          }, {
            "preset": ["shared"],
            "postset": ["shared"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p & o ~= q",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }, {
              "preset": ["exclusive"],
              "postset": ["shared"]
            }, {
              "preset": ["dirty"],
              "postset": ["shared"]
            }, {
              "preset": ["shared"],
              "postset": ["shared"]
            }, {
              "preset": ["dirtyshared"],
              "postset": ["shared"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "p": [
          {
            "preset": ["exclusive"],
            "postset": ["dirty"]
          }, {
            "preset": ["dirty"],
            "postset": ["dirty"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }, {
              "preset": ["exclusive"],
              "postset": ["shared"]
            }, {
              "preset": ["dirty"],
              "postset": ["shared"]
            }, {
              "preset": ["shared"],
              "postset": ["shared"]
            }, {
              "preset": ["dirtyshared"],
              "postset": ["shared"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "guard": "p ~= q",
        "p": [
          {
            "preset": ["shared"],
            "postset": ["dirtyshared"]
          }
        ],
        "q": [
          {
            "preset": ["dirtyshared"],
            "postset": ["shared"]
          }, {
            "preset": ["shared"],
            "postset": ["shared"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p & o ~= q",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }, {
              "preset": ["exclusive"],
              "postset": ["shared"]
            }, {
              "preset": ["dirty"],
              "postset": ["shared"]
            }, {
              "preset": ["shared"],
              "postset": ["shared"]
            }, {
              "preset": ["dirtyshared"],
              "postset": ["shared"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "p": [
          {
            "preset": ["shared"],
            "postset": ["dirty"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }, {
              "preset": ["exclusive"],
              "postset": ["shared"]
            }, {
              "preset": ["dirty"],
              "postset": ["shared"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "guard": "p ~= q",
        "p": [
          {
            "preset": ["dirtyshared"],
            "postset": ["dirtyshared"]
          }
        ],
        "q": [
          {
            "preset": ["dirtyshared"],
            "postset": ["shared"]
          }, {
            "preset": ["shared"],
            "postset": ["shared"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p & o ~= q",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }, {
              "preset": ["exclusive"],
              "postset": ["shared"]
            }, {
              "preset": ["dirty"],
              "postset": ["shared"]
            }, {
              "preset": ["shared"],
              "postset": ["shared"]
            }, {
              "preset": ["dirtyshared"],
              "postset": ["shared"]
            }
          ]
        }
      ]
    }, {
      "rendezvous": {
        "p": [
          {
            "preset": ["dirtyshared"],
            "postset": ["dirty"]
          }
        ]
      },
      "broadcasts": [
        {
          "guard": "o ~= p",
          "changes": [
            {
              "preset": ["invalid"],
              "postset": ["invalid"]
            }, {
              "preset": ["exclusive"],
              "postset": ["shared"]
            }, {
              "preset": ["dirty"],
              "postset": ["shared"]
            }
          ]
        }
      ]
    }
  ]
}
