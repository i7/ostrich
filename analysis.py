import dataclasses
import pathlib
import system
import logging
import typing
import mona
import multiprocessing
import tempfile
import parser
import abduction
import formula


logger = logging.getLogger(__name__)


class Step(typing.NamedTuple):
    counter_example: mona.Model_T
    trap: typing.Optional[mona.Model_T]
    flow: typing.Optional[mona.Model_T]
    siphon: typing.Optional[mona.Model_T]
    info: typing.Optional[str]


Log = typing.Tuple[Step]


@dataclasses.dataclass(frozen=True)
class SystemAutomata:
    intersects: pathlib.Path
    uniquely_intersects: pathlib.Path
    initially_marked: pathlib.Path
    initially_empty: pathlib.Path
    uniquely_initially_marked: pathlib.Path

    initial_marking: pathlib.Path


@dataclasses.dataclass(frozen=True)
class Invalidators:
    trap_invalidator: pathlib.Path
    flow_invalidator: pathlib.Path
    siphon_invalidator: pathlib.Path



@dataclasses.dataclass
class Restrictions:
    system: system.System
    analysis_directory: str

    def __post_init__(self):
        self._trap_formulae: typing.List[str] = []
        self._flow_formulae: typing.List[str] = []
        self._siphon_formulae: typing.List[str] = []
        self._invariants: typing.List[str] = []

        # instantiates trap, flow and siphon automata
        self._construct_trap_automaton()
        self._construct_flow_automaton()
        self._construct_siphon_automaton()

    def list_formulae(self) -> str:
        return "\n".join(
                ["traps: " + ", ".join(self._trap_formulae),
                 "flows: " + ", ".join(self._flow_formulae),
                 "siphons: " + ", ".join(self._siphon_formulae),
                 "invariants: " + ", ".join(self._invariants)])

    @property
    def trap_amount(self) -> int:
        return len(self._trap_formulae)

    @property
    def flow_amount(self) -> int:
        return len(self._flow_formulae)

    @property
    def siphon_amount(self) -> int:
        return len(self._siphon_formulae)

    def pop_trap(self, index: int) -> str:
        trap = self._trap_formulae.pop(index)
        self._construct_trap_automaton()
        return trap

    def pop_flow(self, index: int) -> str:
        flow = self._flow_formulae.pop(index)
        self._construct_flow_automaton()
        return flow

    def pop_siphon(self, index: int) -> str:
        siphon = self._siphon_formulae.pop(index)
        self._construct_siphon_automaton()
        return siphon

    def add_trap(self, formula: str):
        self.add_traps([formula])

    def add_traps(self, formulae: typing.Collection[str]):
        for formula in formulae:
            self._trap_formulae.append(formula)
        self._construct_trap_automaton()

    def add_flow(self, formula: str):
        self.add_flows([formula])

    def add_flows(self, formulae: typing.Collection[str]):
        for formula in formulae:
            self._flow_formulae.append(formula)
        self._construct_flow_automaton()

    def add_siphon(self, formula: str):
        self.add_siphons([formula])

    def add_siphons(self, formulae: typing.Collection[str]):
        for formula in formulae:
            self._siphon_formulae.append(formula)
        self._construct_siphon_automaton()

    def add_invariant(self, formula: str):
        self.add_invariants([formula])

    def add_invariants(self, formulae: typing.Collection[str]):
        self._invariants += formulae

    @property
    def invariants(self):
        return self._invariants

    @property
    def trap_automaton(self) -> pathlib.Path:
        return pathlib.Path(self.analysis_directory, "trap.dfa")

    @property
    def flow_automaton(self) -> pathlib.Path:
        return pathlib.Path(self.analysis_directory, "flow.dfa")

    @property
    def siphon_automaton(self) -> pathlib.Path:
        return pathlib.Path(self.analysis_directory, "siphon.dfa")

    def _construct_trap_automaton(self) -> None:
        template = mona.env.get_template("traps.mona")
        mona.check_formula(template.render(
            system=self.system,
            trap_formulae=self._trap_formulae,
            automaton_name=self.trap_automaton))

    def _construct_flow_automaton(self) -> None:
        template = mona.env.get_template("flows.mona")
        mona.check_formula(template.render(
            system=self.system,
            flow_formulae=self._flow_formulae,
            automaton_name=self.flow_automaton))

    def _construct_siphon_automaton(self) -> None:
        template = mona.env.get_template("siphons.mona")
        mona.check_formula(template.render(
            system=self.system,
            siphon_formulae=self._siphon_formulae,
            automaton_name=self.siphon_automaton))


@dataclasses.dataclass(frozen=True)
class Analysis:
    filepath: pathlib.Path

    def __post_init__(self):
        logger.debug(f"Parsing {self.filepath}.")
        object.__setattr__(self, "system", parser.parse_file(self.filepath))

        logger.debug(f"Starting analysis of {self.filepath}")
        tmp_directory = tempfile.mkdtemp()
        object.__setattr__(self, "_tmp_dir", tmp_directory)
        logger.debug(f"Created temporary directory {self._tmp_dir} to "
                     f"store system-automata files.")
        # intersection automaton
        intersect_dfa = pathlib.Path(self._tmp_dir,
                                     f"{self.filepath.name}-intersects.dfa")
        logger.debug("Creating intersects automaton")
        mona.check_formula(
                mona.env.get_template("intersects.mona").render(
                    system=self.system,
                    automaton_name=str(intersect_dfa)))

        # unique_intersection automaton
        uniquely_intersect_dfa = pathlib.Path(
                self._tmp_dir,
                f"{self.filepath.name}-uniquely-intersects.dfa")
        logger.debug("Creating uniquely intersects automaton")
        mona.check_formula(
                mona.env.get_template("uniquely-intersects.mona").render(
                    system=self.system,
                    automaton_name=str(uniquely_intersect_dfa)))

        # initially empty automaton
        initially_empty_dfa = pathlib.Path(
                self._tmp_dir,
                f"{self.filepath.name}-initially-empty.dfa")
        logger.debug("Creating initially empty automaton")
        mona.check_formula(
                mona.env.get_template("initially-empty.mona").render(
                    system=self.system,
                    automaton_name=str(initially_empty_dfa)))

        # initially marked automaton
        initially_marked_dfa = pathlib.Path(
                self._tmp_dir,
                f"{self.filepath.name}-initially-marked.dfa")
        logger.debug("Creating initially marked automaton")
        mona.check_formula(
                mona.env.get_template("initially-marked.mona").render(
                    system=self.system,
                    automaton_name=str(initially_marked_dfa)))

        # uniquely initially marked automaton
        uniquely_initially_marked_dfa = pathlib.Path(
                self._tmp_dir,
                f"{self.filepath.name}-uniquely-initially-marked.dfa")
        logger.debug("Creating uniquely initially marked automaton")
        mona.check_formula(
                mona.env.get_template("uniquely-initially-marked.mona").render(
                    system=self.system,
                    automaton_name=str(uniquely_initially_marked_dfa)))

        # initial marking
        initial_marking_dfa = pathlib.Path(
                self._tmp_dir,
                f"{self.filepath.name}-initial-marking.dfa")
        logger.debug("Creating initial marking automaton")
        mona.check_formula(
                mona.env.get_template("initial-marking.mona").render(
                    system=self.system,
                    automaton_name=str(initial_marking_dfa)))

        automata = SystemAutomata(
            intersect_dfa,
            uniquely_intersect_dfa,
            initially_marked_dfa,
            initially_empty_dfa,
            uniquely_initially_marked_dfa,
            initial_marking_dfa)

        # trap invalidator
        trap_dfa = pathlib.Path(self._tmp_dir,
                                f"{self.filepath.name}-trap-invalidator.dfa")
        mona.check_formula(
                mona.env.get_template("trap-invalidator.mona").render(
                    system=self.system,
                    broadcast_variable=system.broadcast_variable,
                    automata=automata,
                    automaton_name=str(trap_dfa)))

        # flow invalidator
        flow_dfa = pathlib.Path(self._tmp_dir,
                                f"{self.filepath.name}-flow-invalidator.dfa")
        mona.check_formula(
                mona.env.get_template("flow-invalidator.mona").render(
                    system=self.system,
                    broadcast_variable=system.broadcast_variable,
                    broadcast_variable_snd=system.broadcast_variable_snd,
                    broadcast_variable_thrd=system.broadcast_variable_thrd,
                    automata=automata,
                    automaton_name=str(flow_dfa)))

        # trap invalidator
        siphon_dfa = pathlib.Path(
                self._tmp_dir, f"{self.filepath.name}-siphon-invalidator.dfa")
        mona.check_formula(
                mona.env.get_template("siphon-invalidator.mona").render(
                    system=self.system,
                    broadcast_variable=system.broadcast_variable,
                    automata=automata,
                    automaton_name=str(siphon_dfa)))

        invalidators = Invalidators(
            trap_dfa,
            flow_dfa,
            siphon_dfa)

        object.__setattr__(self, "automata", automata)
        object.__setattr__(self, "invalidators", invalidators)
        object.__setattr__(self, "_induction_pool", multiprocessing.Pool(None))

        # check if given cover is valid:
        cover_invariants: typing.List[str] = []
        # add cover
        for cover_elem in self.system.cover:
            cover_inv = "all1 i: (in_size(i) => ({}))".format(
                    " | ".join(
                        "("
                        + " & ".join(
                            "i {elem} {state}".format(
                                elem=("notin" if state != hit else "in"),
                                state=state)
                            for state in cover_elem)
                        + ")"
                        for hit in cover_elem))
            cover_invariants.append(cover_inv)
        all_covers = " & ".join(cover_invariants)
        init_ce, ind_ce = self.check_invariant(all_covers)
        if init_ce:
            raise ValueError(f"Invariant {all_covers} does not hold in "
                             f"initial marking {init_ce}.")
        elif ind_ce:
            raise ValueError(f"Invariant {all_covers} can be violated from "
                             f"marking {ind_ce} in one step.")
        object.__setattr__(self, "cover_invariants", tuple(cover_invariants))

        # verify topology of system
        for t in self.system.transitions:
            if self.system.topology == system.Topology.RING:
                logger.debug("Checking <RING> topology.")
                if (len(t.rendezvous.changing_terms) == 1
                        and len(t.broadcasts) == 0
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    var1, = t.rendezvous.changing_terms
                    var2 = None
                elif (len(t.rendezvous.changing_terms) == 2
                        and len(t.broadcasts) == 0
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    var1, var2 = t.rendezvous.changing_terms
                else:
                    raise ValueError(f"Transition {t} violates syntactic "
                                     f"restrictions of system topology "
                                     f"<RING>.")
                res = mona.check_formula(
                        mona.env.get_template("ring-topology.mona").render(
                            system=self.system,
                            transition=t,
                            var1=var1,
                            var2=var2))
                if not "Formula is valid" in res:
                    raise ValueError(f"Transition {t} cannot be validated for "
                                     f"topology <RING>")

            elif self.system.topology == system.Topology.HEADEDRING:
                logger.debug("Checking <HEADED-RING> topology.")
                if (len(t.rendezvous.changing_terms) == 1
                        and len(t.broadcasts) == 0):
                    var1, = t.rendezvous.changing_terms
                    var2 = None
                elif (len(t.rendezvous.changing_terms) == 2
                        and len(t.broadcasts) == 0):
                    var1, var2 = t.rendezvous.changing_terms
                else:
                    raise ValueError(f"Transition {t} violates syntactic "
                                     f"restrictions of system topology "
                                     f"<HEADED-RING>.")
                res = mona.check_formula(
                        mona.env.get_template(
                            "headed-ring-topology.mona").render(
                                system=self.system,
                                transition=t,
                                var1=var1,
                                var2=var2,))
                if not "Formula is valid" in res:
                    raise ValueError(f"Transition {t} cannot be validated for "
                                     f"topology <HEADED RING>")
            elif self.system.topology == system.Topology.ARRAY:
                logger.debug("Checking <ARRAY> topology.")
                if (len(t.rendezvous.changing_terms) == 1
                        and len(t.broadcasts) == 1
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    broadcast, = t.broadcasts
                    var1, = t.rendezvous.changing_terms
                    var2 = None
                elif (len(t.rendezvous.changing_terms) == 2
                        and len(t.broadcasts) == 0
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    broadcast = None
                    var1, var2 = t.rendezvous.changing_terms
                elif (len(t.rendezvous.changing_terms) == 1
                        and len(t.broadcasts) == 0
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    broadcast = None
                    var1, = t.rendezvous.changing_terms
                    var2 = None
                else:
                    raise ValueError(f"Transition {t} violates syntactic "
                                     f"restrictions of system topology: "
                                     f"<ARRAY>")
                res = mona.check_formula(
                        mona.env.get_template("array-topology.mona").render(
                            system=self.system,
                            broadcast_variable=system.broadcast_variable,
                            broadcast=broadcast,
                            transition=t,
                            var1=var1,
                            var2=var2))
                if not "Formula is valid" in res:
                    raise ValueError(f"Transition {t} cannot be validated for "
                                     f"topology <ARRAY>")
            elif self.system.topology == system.Topology.CROWD:
                logger.debug("Checking <CROWD> topology.")
                if (len(t.broadcasts) == 0
                        and len(t.rendezvous.changing_terms) == 1
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    broadcast = None
                    var1, = t.rendezvous.changing_terms
                    var2 = None
                elif (len(t.broadcasts) == 0
                        and len(t.rendezvous.changing_terms) == 2
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    broadcast = None
                    var1, var2 = t.rendezvous.changing_terms
                elif (len(t.broadcasts) == 1
                        and len(t.rendezvous.changing_terms) == 1
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    broadcast, = t.broadcasts
                    var1, = t.rendezvous.changing_terms
                    var2 = None
                elif (len(t.broadcasts) == 1
                        and len(t.rendezvous.changing_terms) == 2
                        and all([isinstance(ct, formula.Variable)
                                 for ct in t.rendezvous.changing_terms])):
                    broadcast, = t.broadcasts
                    var1, var2 = t.rendezvous.changing_terms
                else:
                    raise ValueError(f"Transition {t} violates syntactic "
                                     f"restrictions of system topology: "
                                     f"<CROWD>")
                res = mona.check_formula(
                        mona.env.get_template("crowd-topology.mona").render(
                            system=self.system,
                            broadcast_variable=system.broadcast_variable,
                            broadcast=broadcast,
                            transition=t,
                            var1=var1,
                            var2=var2))
                if not "Formula is valid" in res:
                    raise ValueError(f"Transition {t} cannot be validated for "
                                     f"topology <CROWD>")

    def check_invariant(self, formula: str) -> typing.Tuple[
            typing.Optional[mona.Model_T],
            typing.Optional[mona.Model_T]]:
        # returns:
        #  1) None if all initial markings satisfy formula otherwise a counter
        #     example
        #  2) None if formula is inductive otherwise a marking which satisfies
        #     the property but which can reach a marking which does not in one
        #     step
        throwaway_dir = pathlib.Path(self._tmp_dir, "throwaway")
        throwaway_dir.mkdir(exist_ok=True)
        re = Restrictions(self.system, throwaway_dir)
        return self.check_initial(formula), self.check_inductive(formula, re)

    def check_initial(self, formula: str) -> typing.Optional[mona.Model_T]:
        result = mona.check_formula(
                mona.env.get_template("property-initial.mona").render(
                    system=self.system,
                    formula=formula,
                    automata=self.automata))
        if not "Formula is unsatisfiable" in result:
            return mona.get_model(result)
        else:
            return None

    def check_deadlock(self,
                       restrictions: Restrictions
                      ) -> typing.Optional[mona.Model_T]:
        template = mona.env.get_template("check-deadlock.mona")
        deadlock = template.render(
                system=self.system,
                broadcast_variable=system.broadcast_variable,
                automata=self.automata,
                restrictions=restrictions)
        logger.info("Checking deadlock.")
        result = mona.check_formula(deadlock)
        if "Formula is unsatisfiable" in result:
            return None
        else:
            model = mona.get_model(result)
            return model

    def check_property(self,
                       prop: str,
                       restrictions: Restrictions
                       ) -> typing.Optional[mona.Model_T]:
        if prop == "deadlock":
            return self.check_deadlock(restrictions)
        else:
            return self.check_inductive(
                    f"~({self.system.get_property(prop)})", restrictions)

    def transition_formula(self) -> str:
        connects_template = mona.env.get_template("connects.mona")
        formulae = [connects_template.render(
                        system=self.system,
                        broadcast_variable=system.broadcast_variable,
                        transition=t,
                        transition_index=index)
                    for index, t in enumerate(self.system.transitions)]
        return "\n".join(formulae)


    def check_inductive(self,
                        formula: str,
                        restrictions: Restrictions
                        ) -> typing.Optional[mona.Model_T]:
        connects_template = mona.env.get_template("connects.mona")
        template = mona.env.get_template("property-inductive.mona")
        logger.debug(f"Checking inductiveness of {formula} under restrictions:"
                     f" {restrictions.list_formulae()}")
        formulae = [
                template.render(
                    system=self.system,
                    broadcast_variable=system.broadcast_variable,
                    property=formula,
                    restrictions=restrictions,
                    connects_formula=connects_template.render(
                        system=self.system,
                        broadcast_variable=system.broadcast_variable,
                        transition=t,
                        transition_index=index,
                    ),
                    transition_index=index,
                    automata=self.automata
                    ) for index, t in enumerate(self.system.transitions)]
        logger.info(f"Checking inductiveness of formula <{formula}>.")
        results = self._induction_pool.map(mona.check_formula, formulae)
        for counter_example in results:
            if "Formula is unsatisfiable" in counter_example:
                continue
            else:
                return mona.get_model(counter_example)
        return None

    def invalidate_by_trap(self,
                           marking: mona.Model_T
                          ) -> typing.Optional[mona.Model_T]:
        template = mona.env.get_template("invalidate-marking.mona")
        trap = template.render(
                system=self.system,
                marking=marking,
                variables=self.system.trap_states,
                invalidator_name=self.invalidators.trap_invalidator)
        logger.info("Invalidate by trap.")
        result = mona.check_formula(trap)
        if "Formula is unsatisfiable" in result:
            return None
        else:
            model = mona.get_model(result)
            return {**{var: model[var] for var in self.system.trap_states},
                    "n": model["n"]}

    def invalidate_by_siphon(self,
                             marking: mona.Model_T
                            ) -> typing.Optional[mona.Model_T]:
        template = mona.env.get_template("invalidate-marking.mona")
        siphon = template.render(
                system=self.system,
                marking=marking,
                variables=self.system.siphon_states,
                invalidator_name=self.invalidators.siphon_invalidator)
        logger.info("Invalidate by siphon.")
        result = mona.check_formula(siphon)
        if "Formula is unsatisfiable" in result:
            return None
        else:
            model = mona.get_model(result)
            return {**{var: model[var] for var in self.system.siphon_states},
                    "n": model["n"]}

    def invalidate_by_flow(self,
                           marking: mona.Model_T
                          ) -> typing.Optional[mona.Model_T]:
        template = mona.env.get_template("invalidate-marking.mona")
        trap = template.render(
                system=self.system,
                marking=marking,
                variables=self.system.flow_states,
                invalidator_name=self.invalidators.flow_invalidator)
        logger.info("Invalidate by flow.")
        result = mona.check_formula(trap)
        if "Formula is unsatisfiable" in result:
            return None
        else:
            model = mona.get_model(result)
            return {**{var: model[var] for var in self.system.flow_states},
                    "n": model["n"]}

    def __del__(self):
        if hasattr(self, "_induction_pool"):
            self._induction_pool.close()
            self._induction_pool.terminate()


@dataclasses.dataclass
class Generalization:
    analysis: Analysis
    prop: str

    def __post_init__(self):
        self.directory: str = tempfile.mkdtemp()
        self.log: typing.List[Step] = []
        self.restrictions = Restrictions(self.analysis.system, self.directory)
        self.restrictions.add_invariants(self.analysis.cover_invariants)

        self.result = None
        # check if property holds initially
        if self.prop != "deadlock":
            ce = self.analysis.check_initial(
                    f"~({self.analysis.system.get_property(self.prop)})")
            if ce:
                self.log.append(Step(ce,
                                     None,
                                     None,
                                     None,
                                     f"Property {prop} is initially violated.")
                                )
                self.result = False


    def check(self) -> None:
        logger.info(f"Checking property {self.prop}.")
        if self.result is not None:
            # Already established a result; we do not run twice then
            return

        # Check property under these restrictions
        while self.result is None:
            ce = self.analysis.check_property(self.prop, self.restrictions)
            logger.info(f"Counter example for {self.prop}: {ce}")
            self.disprove_counter_example(ce)

    def remove_unused_invariants(self):
        # try to reduce the amount of invariants
        # flows:
        for _ in range(self.restrictions.flow_amount):
            removed_flow = self.restrictions.pop_flow(0)
            if self.analysis.check_property(self.prop, self.restrictions):
                self.restrictions.add_flow(removed_flow)
        # traps:
        for _ in range(self.restrictions.trap_amount):
            removed_trap = self.restrictions.pop_trap(0)
            if self.analysis.check_property(self.prop, self.restrictions):
                self.restrictions.add_trap(removed_trap)
        # siphons:
        for _ in range(self.restrictions.siphon_amount):
            removed_siphon = self.restrictions.pop_siphon(0)
            if self.analysis.check_property(self.prop, self.restrictions):
                self.restrictions.add_siphon(removed_siphon)


    def disprove_counter_example(self, ce: mona.Model_T) -> None:
        # if there is no counter example we are done
        if ce is None:
            self.result = True
            return

        # otherwise:
        # try to invalidate by flow:
        if flow := self.analysis.invalidate_by_flow(ce):
            logger.debug(f"Flow <{flow}> invalidates <{ce}>")
            flow_formulae = abduction.abduct(flow, self.analysis.system)
            logger.debug(f"Generalizing <{flow}> to:\n" + "\n".join(
                flow_formulae.formulae))
            self.restrictions.add_flows(flow_formulae.formulae)
            self.log.append(Step(ce,
                                 None,
                                 flow,
                                 None,
                                 "adding new flow generator: {}".format(
                                     ", ".join(flow_formulae.formulae))))
        # try to invalidate by trap:
        elif trap := self.analysis.invalidate_by_trap(ce):
            logger.debug(f"Trap <{trap}> invalidates <{ce}>")
            trap_formulae = abduction.abduct(trap, self.analysis.system)
            logger.debug(f"Generalizing <{trap}> to:\n" + "\n".join(
                trap_formulae.formulae))
            self.restrictions.add_traps(trap_formulae.formulae)
            self.log.append(Step(ce,
                                 trap,
                                 None,
                                 None,
                                 "adding new trap generator: " "{}".format(
                                     ", ".join(trap_formulae.formulae))))
        elif siphon := self.analysis.invalidate_by_siphon(ce):
            logger.debug(f"Siphon <{siphon}> invalidates <{ce}>")
            siphon_formulae = abduction.abduct(siphon, self.analysis.system)
            logger.debug(f"Generalizing <{siphon}> to:\n" + "\n".join(
                siphon_formulae.formulae))
            self.restrictions.add_siphons(siphon_formulae.formulae)
            self.log.append(Step(ce,
                                 None,
                                 None,
                                 siphon,
                                 "adding new siphon generator: {}".format(
                                     ", ".join(siphon_formulae.formulae))))
        # this is trap, flow, and siphon hard; give up:
        else:
            self.log.append(Step(
                ce,
                None,
                None,
                None,
                "Cannot invalidate counter example with traps or flows."))
            self.result = False

    def jsonify(self) -> typing.Dict:
        json_dict = {}
        json_dict["result"] = self.result
        json_dict["final sets"] = {
                "traps": self.restrictions._trap_formulae,
                "flows": self.restrictions._flow_formulae,
                "siphons": self.restrictions._siphon_formulae,
                "invariants": self.restrictions.invariants}
        def dictionarize_step(step):
            step_dict = {}
            step_dict["counter example"] = str(step.counter_example)
            if step.trap:
                step_dict["trap"] = str(step.trap)
            elif step.flow:
                step_dict["flow"] = str(step.flow)
            elif step.siphon:
                step_dict["siphon"] = str(step.siphon)
            step_dict["info"] = step.info
            return step_dict
        json_dict["log"] = [dictionarize_step(step) for step in self.log]
        return json_dict
