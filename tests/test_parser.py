import unittest
from parser import parse_formula as parse
import formula


class ParserTest(unittest.TestCase):
    def test_atoms(self):
        self.assertEqual(parse("1 = 2"),
                         formula.Equality(
                             formula.Constant(1),
                             formula.Constant(2)))
        self.assertEqual(parse("1 ~= 2"),
                         formula.Negation(
                             formula.Equality(
                                 formula.Constant(1),
                                 formula.Constant(2))))
        self.assertEqual(parse("a < b"),
                         formula.Smaller(
                             formula.Variable("a"),
                             formula.Variable("b")))
        self.assertEqual(parse("a <= b"),
                         formula.Disjunction(
                             formula.Smaller(
                                 formula.Variable("a"),
                                 formula.Variable("b")),
                             formula.Equality(
                                 formula.Variable("a"),
                                 formula.Variable("b"))))

    def test_precedence(self):
        self.assertEqual(
                parse("~(a < b | 1 = 2) & c >> d | ~ a >> b"),
                formula.Disjunction(
                    formula.Conjunction(
                        formula.Negation(
                            formula.Disjunction(
                                formula.Smaller(
                                    formula.Variable("a"),
                                    formula.Variable("b")),
                                formula.Equality(
                                    formula.Constant(1),
                                    formula.Constant(2)))),
                        formula.IsNext(
                            formula.Variable("c"),
                            formula.Variable("d"))),
                    formula.Negation(
                        formula.IsNext(
                            formula.Variable("a"),
                            formula.Variable("b")))))


if __name__ == '__main__':
    unittest.main()
