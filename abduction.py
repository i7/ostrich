import dataclasses
import typing
import mona
import system
import logging
from collections import Counter


logger = logging.getLogger(__name__)


View = typing.Tuple[typing.Tuple[str, ...], ...]


def get_view(model: mona.Model_T, in_system: system.System) -> View:
    lookup = {var[1:]: model[var] for var in model if var != "n"}
    return tuple(tuple(state for state in in_system.states
                       if index in lookup[state])  # type: ignore
                 for index in range(model["n"]))  # type: ignore

def abduct(model: mona.Model_T, in_system: system.System) -> "Abduction":
    view = get_view(model, in_system)
    if in_system.topology == system.Topology.RING:
        return RingAbduction(view, in_system)
    elif in_system.topology == system.Topology.HEADEDRING:
        return HeadedRingAbduction(view, in_system)
    elif in_system.topology == system.Topology.ARRAY:
        return ArrayAbduction(view, in_system)
    elif in_system.topology == system.Topology.CROWD:
        return CrowdAbduction(view, in_system)
    else:
        raise ValueError(f"Cannot abduct for topology {in_system.topology}")



@dataclasses.dataclass(frozen=True)
class Abduction:
    view: View
    system: system.System

    def __post_init__(self):
        import operator
        from functools import reduce

        N = len(self.view)
        universe = tuple(range(N))
        indices = tuple(i for i in universe if self.view[i])
        local = self.check_local()
        object.__setattr__(self, "_N", N)
        object.__setattr__(self, "_local", local)
        object.__setattr__(self, "_indices", indices)
        object.__setattr__(self, "_universe", universe)

        formulae = self.construct_formulae()
        object.__setattr__(self, "_formulae", formulae)

    def render_artifact(self) -> str:
        # add specifically this set:
        pre = f"n = {self.N}"
        artifact = " & ".join(
                f"{state} = " + ("{"
                                 + ", ".join(str(i)
                                             for i in self.get_state(state))
                                 + "}" if self.get_state(state) else "empty")
                for state in self.system.states)
        return f"{pre} & {artifact}"

    def _index_view(self, index: int) -> typing.Tuple[str, ...]:
        return self.view[index]

    def check_local(self) -> bool:
        return not all(self.view)

    def get_state(self, state: str) -> typing.Tuple[int, ...]:
        acc = []
        for index, letter in enumerate(self.view):
            if state in letter:
                acc.append(index)
        return tuple(acc)

    @property
    def universe(self) -> typing.Tuple[int, ...]:
        return self._universe  # type: ignore

    @property
    def local(self) -> bool:
        return self._local  # type: ignore

    @property
    def indices(self) -> typing.Tuple[int, ...]:
        return self._indices  # type: ignore

    @property
    def N(self) -> int:
        return self._N  # type: ignore

    @property
    def formulae(self) -> typing.List[str]:
        return self._formulae  # type: ignore

    def construct_formulae(self) -> typing.List[str]:
        if self.local:
            return self.local_formulae()
        else:
            return self.global_formulae()

    def local_formulae(self) -> typing.List[str]:
        # this should be overwritten in subclasses; otherwise there is no
        # refining of the abstraction whatsoever
        return [self.render_artifact()]

    def global_formulae(self) -> typing.List[str]:
        # this should be overwritten in subclasses; otherwise there is no
        # refining of the abstraction whatsoever
        return [self.render_artifact()]


class RingAbduction(Abduction):
    def local_adjacent_formula(self, adjacent_view: View):
        view_N = len(adjacent_view)
        indices = ", ".join(f"i{i}" for i in range(view_N))
        index_condition = " & ".join([f"is_next(i{i}, i{i+1})"
                                      for i in range(view_N-1)]
                                     + ["in_size(i0)"])
        post_atoms = []
        for state in self.system.states:
            acc = []
            for i, index_set in enumerate(adjacent_view):
                if state in index_set:
                    acc.append(f"i{i}")
            post_atoms.append(f"{state} = " + ("{" + ", ".join(acc) + "}"
                                               if acc else "empty"))
        post = " & ".join(post_atoms)
        return (f"{view_N} <= n & (ex1 {indices}: {index_condition} & {post})")

    def local_formulae(self) -> typing.List[str]:
        # rotate view to get index 0 to be start of an adjacent trap:
        rotated_view = list(self.view)
        while not (rotated_view[0] and not rotated_view[-1]):
            rotated_view = rotated_view[1:] + [rotated_view[0]]

        # decompose into adjacent traps
        adjacent_traps = []
        while rotated_view:
            adjacent_trap = []
            # take beginning trap
            while rotated_view and rotated_view[0]:
                adjacent_trap.append(rotated_view.pop(0))
            adjacent_traps.append(adjacent_trap)
            # when it stops drop gap
            while rotated_view and not rotated_view[0]:
                rotated_view.pop(0)

        adjacent_formulae = [self.local_adjacent_formula(tuple(at))
                             for at in adjacent_traps]
        return adjacent_formulae

    def global_formulae(self) -> typing.List[str]:
        import math
        compressed_view = []
        i = 0
        while i < len(self.view):
            for wl in range(1, math.ceil(len(self.view[i:]) / 2) + 1):
                w1 = self.view[i:i+wl]
                w2 = self.view[i+wl:i+2*wl]
                if w1 == w2:
                    # found repeating pattern; adding
                    pattern = self.view[i:i+wl]
                    compressed_view.append((pattern, True))
                    # jump i passed this pattern
                    while (i + wl <= len(self.view)
                            and self.view[i:i+wl] == pattern):
                        i += wl
                    break
            else:
                compressed_view.append((self.view[i:i+1], False))
                i += 1
        logger.debug(f"view: {self.view} compressed to: {compressed_view}")

        loopback_views: typing.List[typing.Tuple[typing.Tuple[str, ...],
                                                 typing.Optional[int]]] = []
        for window, repeating in compressed_view:
            for view in window[:-1]:
                loopback_views.append((view, None))
            if not repeating:
                loopback_views.append((window[-1], None))
            else:
                loopback_views.append((window[-1], -len(window)+1))

        indices = [f"P{i}" for i in range(len(loopback_views))]
        # we start in the first index and end in the last
        index_cond = ["(0 in P0)"] + [f"(n-1 in {indices[-1]})"]
        # we either loop in a pattern or advance to the next
        for i, (view, loopback) in enumerate(loopback_views):
            if i == len(indices)-1:
                following_index = indices[0]
            else:
                following_index = indices[i+1]
            if loopback is not None:
                following_index += " union " + indices[loopback]
            index_cond.append(
                    ("((j in {current_index}) "
                     "=> ((j+1 in {following_index}) & ({view})))").format(
                         current_index=indices[i],
                         following_index=following_index,
                         view=" & ".join(
                             "j {op} {state}".format(
                                 state=state,
                                 op="in" if state in view else "notin")
                             for state in self.system.states)))

        # the set of indices partitions in_size
        partition = " | ".join(
                "(" + " & ".join([f"j in {hit}"] + [f"j notin {miss}"
                                                    for miss in indices
                                                    if miss != hit]) + ")"
                for hit in indices)

        # assemble
        comment = "/*("
        for comp_view, repeating in compressed_view:
            comment += "(" + "".join("{" + ", ".join(v for v in view) + "}"
                                     for view in comp_view) + ")"
            if repeating:
                comment += "+"
        comment +=")+*/"
        return [("{comment}({N} <= n) & ex2 {indices}: "
                 "((all1 j: in_size(j) => (({parition}) & ({post}))))").format(
                     comment=comment,
                     N=len(indices),
                     indices=", ".join(indices),
                     parition=partition,
                     post=" & ".join(index_cond))]


class HeadedRingAbduction(Abduction):
    def check_local(self):
        return not all(self.view[2:-1])

    def global_formulae(self) -> typing.List[str]:
        # if too small return as artifact
        if self.N < 5:
            return [self.render_artifact()]

        # else extract "special" elements:
        last = self.view[-1]
        zero = self.view[0]
        one = self.view[1]

        remainder = self.view[2:-1]
        # find periodicity in remainder
        period = None
        length = len(remainder)
        possible_periods = [i for i in range(1, length + 1) if length % i == 0]
        for j in possible_periods:
            decomposition = [
                    remainder[j*k:(k+1)*j] for k in range(int(length / j))]
            if len(set(decomposition)) == 1:
                period = j
                break
        else:
            # there is no periodicty:
            return [self.render_artifact()]
        periodic_view = remainder[:period]
        special_sets = " & ".join(
                [f"0 notin {miss}"
                 for miss in self.system.states if miss not in zero]
                + [f"0 in {hit}"
                   for hit in zero]
                + [f"1 notin {miss}"
                   for miss in self.system.states if miss not in one]
                + [f"1 in {hit}"
                   for hit in one]
                + [f"n-1 notin {miss}"
                   for miss in self.system.states if miss not in last]
                + [f"n-1 in {hit}"
                   for hit in last])
        if period == 1:
            # all in remainder share the same structure
            empty_states = [state for state in self.system.states
                            if state not in periodic_view[0]]
            occupied_states = [state for state in self.system.states
                               if state in periodic_view[0]]
            inner_sets = " & ".join([f"i notin {miss}"
                                     for miss in empty_states]
                                    + [f"i in {hit}"
                                       for hit in occupied_states])
            return [(f"(4 <= n) & {special_sets} & all1 i: ((1 < i & i < n-1) "
                     f"=> ({inner_sets}))")]
        else:
            # period is non-trivial and we need to render an index set which
            # allows to identify the period
            period_condition = ("2 in period & all1 i: ((2 <= i & i <= n-1)=>"
                                "(i in period <=> ({})))").format(
                                        " & ".join([f"i + {k} notin period"
                                         for k in range(1, period)]
                                        + [f"i + {period} in period"]))
            indices = ", ".join(f"i{i}" for i in range(period))
            indices_condition = " & ".join([f"i{i+1} = i{i} + 1"
                                            for i in range(period-1)]
                                           + ["i0 in period", "i0 ~= n-1"])
            post_atoms = []
            for i in range(period):
                post_atoms += [f"i{i} notin {miss}"
                               for miss in self.system.states
                               if miss not in remainder[i]]
                post_atoms += [f"i{i} in {hit}"
                               for hit in remainder[i]]
            post = " & ".join(post_atoms)
            return [(f"{period+3} <= n & {special_sets} & ("
                     f"ex2 period: ({period_condition} & n-1 in period) & ("
                     f"all1 {indices}: (({indices_condition}) => ({post}))))")]

    def local_formulae(self):
        def non_empty(v):
            index, view = v
            ret = bool(index in {0, 1, self.N} or view)
            return ret
        # construct view with annotated indices
        indexed_view = list(enumerate(self.view))
        # rotate until the last element is empty and the first not
        while non_empty(indexed_view[-1]) or not non_empty(indexed_view[0]):
            indexed_view = indexed_view[1:] + [indexed_view[0]]

        adjacent_views = []
        while indexed_view:
            adjacent_view = []
            while indexed_view and non_empty(indexed_view[0]):
                adjacent_view.append(indexed_view.pop(0))
            adjacent_views.append(adjacent_view)
            while indexed_view and not (non_empty(indexed_view[0])):
                indexed_view.pop(0)
        return [self.local_adjacent_formula(av)
                for av in adjacent_views]

    def local_adjacent_formula(
            self,
            adjacent_view: typing.List[typing.Tuple[int,
                                                    typing.Tuple[str, ...]]]
            ) -> str:
        view_N = len(adjacent_view)
        indices = ", ".join(f"i{i}" for i in range(view_N))
        indices_condition = " & ".join(
                [f"is_next(i{i}, i{i+1})" for i in range(view_N - 1)]
                + [f"i{i} = {k}" for i, (k, _) in enumerate(adjacent_view)
                   if k in {0, 1}]
                + [f"i{i} = n-1" for i, (k, _) in enumerate(adjacent_view)
                   if k == self.N - 1]
                + [f"i{i} ~= 0 & i{i} ~= 1 & i{i} ~= n-1"
                   for i, (k, _) in enumerate(adjacent_view)
                   if k not in {0, 1, self.N - 1}]
                + ["in_size(i0)"])
        post_atoms = []
        for state in self.system.states:
            acc = []
            for i, (_, view) in enumerate(adjacent_view):
                if state in view:
                    acc.append(f"i{i}")
            post_atoms.append(
                    f"{state} = " + ("{" + ", ".join(acc) + "}" if acc
                                     else "empty"))
        post = " & ".join(post_atoms)
        return f"{view_N} <= n & ex1 {indices}: ({indices_condition} & {post})"


class ArrayAbduction(Abduction):
    def global_formulae(self) -> typing.List[str]:
        compressed_view = []
        index = 0
        while index < len(self.view):
            if self.view[index] and index == len(self.view)-1:
                # unique occurence
                compressed_view.append((self.view[index], False))
                index += 1
            elif self.view[index] and self.view[index] != self.view[index+1]:
                # unique occurence
                compressed_view.append((self.view[index], False))
                index += 1
            elif self.view[index] and self.view[index] == self.view[index+1]:
                # repeating sequence:
                compressed_view.append((self.view[index], True))
                # jump over remainder of pumpable sequence
                index += 1
                while (index < len(self.view)
                        and self.view[index-1] == self.view[index]):
                    index += 1
            else:
                # empty sequence:
                compressed_view.append((self.view[index], True))
                while (index < len(self.view)
                        and not self.view[index]):
                    index += 1

        if not any([pumpable for _, pumpable in compressed_view]):
            return [self.render_artifact()]

        ex_vars = [f"i{i}" for i in range(len(compressed_view))]
        ex_cond = ["i0 = 0"] + [f"in_size(i{i})"
                                for i in range(len(compressed_view))]
        if not compressed_view[-1][1]:
            ex_cond.append(f"i{len(compressed_view)-1} = n-1")
        for index, (_, pumpable) in enumerate(compressed_view):
            if index == len(compressed_view)-1:
                if pumpable:
                    continue
                else:
                    ex_cond.append(f"i{index} = n-1")
            elif pumpable:
                ex_cond.append(f"i{index} < i{index+1}")
            else:
                ex_cond.append(f"i{index}+1 = i{index+1}")

        set_atoms = []
        for index, (places, _) in enumerate(compressed_view):
            cond = (f"i{index} <= j" if index == len(compressed_view)-1
                    else f"i{index} <= j & j < i{index+1}")
            set_atoms.append("(({cond}) => ({desc}))".format(
                cond=cond,
                desc=" & ".join(
                    "j {op} {state}".format(
                        op="in" if state in places else "notin",
                        state=state)
                    for state in self.system.states)))
        return [("{N} <= n & ex1 {ex_vars}: {vars_cond} & "
                 "(all1 j: in_size(j) => ({places}))").format(
                     N=len(compressed_view),
                     ex_vars=", ".join(ex_vars),
                     vars_cond=" & ".join(ex_cond),
                     places=" & ".join(set_atoms))]

    def local_formulae(self):
        # since pumpable empty sets are already incorporated into global
        # formulae it suffices to call that
        return self.global_formulae()


class CrowdAbduction(Abduction):
    @property
    def multiset_view(self) -> typing.Counter[typing.Tuple[str, ...]]:
        return Counter(self.view)

    def check_local(self) -> bool:
        return 2 <= self.multiset_view[tuple()]

    def global_formulae(self) -> typing.List[str]:
        from itertools import combinations
        mv = self.multiset_view
        # extract sets which can be added:
        pumpable_sets = sorted(k for k in mv.keys() if mv[k] > 2)
        if not pumpable_sets:
            return [self.render_artifact()]
        # do sorted(counter.elements()) but with at most 2 consecutive
        # occurences:
        exact_sets = sorted(mv.elements())
        ex_vars = [f"i{i}" for i in range(len(exact_sets))]
        ex_cond = " & ".join([f"in_size({var})" for var in ex_vars]
                             + [f"{var1} ~= {var2}"
                                for var1, var2 in combinations(ex_vars, 2)
                                if var1 != var2])
        ex_atoms = []
        for var, exact_set in zip(ex_vars, exact_sets):
            ex_atoms.append(" & ".join("{var} {in_op} {state}".format(
                var=var,
                in_op="in" if state in exact_set else "notin",
                state=state) for state in self.system.states))
        universal_cond = "in_size(j) & " + " & ".join(f"j ~= {var}"
                                                      for var in ex_vars)
        univ_atoms = []
        for pumpable_set in pumpable_sets:
            inner = " & ".join(
                    "j {in_op} {state}".format(
                        in_op="in" if state in pumpable_set else "notin",
                        state=state)
                    for state in self.system.states)
            univ_atoms.append(f"({inner})")
        return [("(ex1 {ex_vars}: {ex_cond} & {ex_atoms} & "
                 "all1 j: (({universal_cond}) => ({univ_formula})))").format(
                     ex_vars=", ".join(ex_vars),
                     ex_cond=ex_cond,
                     ex_atoms=" & ".join(ex_atoms),
                     universal_cond=universal_cond,
                     univ_formula=" | ".join(univ_atoms))]


    def local_formulae(self) -> typing.List[str]:
        from itertools import combinations
        mv = self.multiset_view
        aggregated_elements = [elem for elem in mv.elements() if elem]
        variables = tuple(f"i{i}" for i in range(len(aggregated_elements)))
        index_condition = " & ".join([f"in_size({var})" for var in variables]
                                     + [f"{var1} ~= {var2}"
                                        for var1, var2 in combinations(
                                            variables, 2)
                                        if var1 != var2])
        post_atoms = []
        for state in self.system.states:
            acc = []
            for var, element in zip(variables, aggregated_elements):
                if state in element:
                    acc.append(var)
            post_atoms.append(f"{state} = " + ("{" + ", ".join(acc) + "}"
                                               if acc else "empty"))
        return ["{N} <= n & (ex1 {indices}: {index_condition} & {post})".format(
            N=self.N,
            indices=", ".join(variables),
            index_condition=index_condition,
            post=" & ".join(post_atoms))]
