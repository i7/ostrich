import cmd
import typing
import parser
import sys
import mona
import analysis
import abduction


class GeneralizationPrompt(cmd.Cmd):
    prompt: str = "> "

    def __init__(self,
                 algorithm: analysis.Generalization,
                 counter_example: mona.Model_T) -> None:
        super().__init__()
        self.algorithm: analysis.Generalization = algorithm
        self.counter_example: mona.Model_T = counter_example
        self.invalidating_trap: typing.Optional[mona.Model_T] = None
        self.invalidating_flow: typing.Optional[mona.Model_T] = None
        self.invalidating_siphon: typing.Optional[mona.Model_T] = None

    @property
    def intro(self):
        return (f"Welcome to this interactive tool for analysis of "
                f"parameterized Petri nets. The current counter example is:\n"
                f"{self.counter_example}")

    def do_trap(self, _):
        self.invalidating_trap = self.algorithm.analysis.invalidate_by_trap(
                self.counter_example)
        if self.invalidating_trap:
            print(f"Trap: {self.invalidating_trap}")
            print(f"invalidates: {self.counter_example}")
        else:
            print(f"Cannot invalidate counter example {self.counter_example} "
                  f"by trap.")

    def do_siphon(self, _):
        self.invalidating_siphon = self.algorithm.analysis.invalidate_by_siphon(
                self.counter_example)
        if self.invalidating_siphon:
            print(f"Trap: {self.invalidating_siphon}")
            print(f"invalidates: {self.counter_example}")
        else:
            print(f"Cannot invalidate counter example {self.counter_example} "
                  f"by siphon.")

    def do_flow(self, _):
        self.invalidating_flow = self.algorithm.analysis.invalidate_by_flow(
                self.counter_example)
        if self.invalidating_flow:
            print(f"Flow: {self.invalidating_flow}")
            print(f"invalidates: {self.counter_example}")
        else:
            print(f"Cannot invalidate counter example {self.counter_example} "
                  f"by flow.")

    def do_generalize(self, arg):
        if arg.lower() == "trap":
            if not self.invalidating_trap:
                print(f"There is no trap to generalize.")
            else:
                generalized_traps = abduction.abduct(
                        self.invalidating_trap, self.algorithm.analysis.system)
                print("Adding {} to trap generators.".format(
                    ", ".join(generalized_traps.formulae)))
                self.algorithm.restrictions.add_traps(
                        generalized_traps.formulae)
                self.algorithm.log.append(analysis.Step(
                    self.counter_example,
                    self.invalidating_trap,
                    None,
                    None,
                    "Abducted trap generator {} for the user.".format(
                        ", ".join(generalized_traps.formulae))))
                return True
        elif arg.lower() == "flow":
            if not self.invalidating_flow:
                print(f"There is no flow to generalize.")
            else:
                generalized_flows = abduction.abduct(
                        self.invalidating_flow, self.algorithm.analysis.system)
                print("Adding {} to flow generators.".format(
                    ", ".join(generalized_flows.formulae)))
                self.algorithm.restrictions.add_flows(
                    generalized_flows.formulae)
                self.algorithm.log.append(analysis.Step(
                    self.counter_example,
                    None,
                    self.invalidating_flow,
                    None,
                    "Abducted flow generator {} for the user.".format(
                        ", ".join(generalized_flows.formulae))))
                return True
        elif arg.lower() == "siphon":
            if not self.invalidating_siphon:
                print(f"There is no siphon to generalize.")
            else:
                generalized_siphons = abduction.abduct(
                        self.invalidating_siphon,
                        self.algorithm.analysis.system)
                print("Adding {} to siphon generators.".format(
                    ", ".join(generalized_siphons.formulae)))
                self.algorithm.restrictions.add_siphons(
                        generalized_traps.formulae)
                self.algorithm.log.append(analysis.Step(
                    self.counter_example,
                    None,
                    None,
                    self.invalidating_siphon,
                    "Abducted siphon generator {} for the user.".format(
                        ", ".join(generalized_siphons.formulae))))
                return True
        else:
            print("Please use generalize either with <trap>, <flow>, or "
                  "<siphon> as argument.")

    def do_invariant(self, invariant):
        try:
            init_ce, ind_ce = self.algorithm.analysis.check_invariant(
                    invariant)
            if init_ce:
                print(f"Invariant {invariant}\nis initially violated in "
                      f"{init_ce}")
            if ind_ce:
                print(f"Invariant {invariant}\ncan be violated from {ind_ce}")
            if init_ce is None and ind_ce is None:
                print(f"Adding inductive invariant: {invariant}.")
                self.algorithm.restrictions.add_invariant(invariant)
                self.algorithm.log.append(analysis.Step(
                    self.counter_example,
                    None,
                    None,
                    None,
                    f"User added {invariant} to the system."))
            return True
        except ChildProcessError as e:
            print(f"Could not establish invariant {invariant} b/c: {e}")

    def do_exit(self, _):
        print("Moving on.")
        return True

    def do_fail(self, _):
        self.algorithm.result = False
        return True

    def do_EOF(self, _):
        return self.do_exit(None)

class InteractiveGeneralizer(analysis.Generalization):
    def disprove_counter_example(self, ce: mona.Model_T) -> None:
        if ce is None:
            self.result = True
            return

        # otherwise ask user for input:
        cmdline = GeneralizationPrompt(self, ce)
        cmdline.cmdloop()
