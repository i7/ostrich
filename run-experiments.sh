#!/bin/env bash

EXPERIMENTS=(
  "examples/rings/atomic-philosopher.json"
  "examples/rings/dijkstra-ring.json"
  "examples/headed-rings/left-hander.json"
  "examples/headed-rings/leader-election.json"
  "examples/crowds/dijkstra-atomic.json"
  "examples/arrays/burns.json"
  "examples/crowds/hy-acurate/berkeley.json"
  "examples/crowds/hy-acurate/firefly.json"
  "examples/crowds/hy-acurate/mesi.json"
  "examples/crowds/hy-acurate/synapse.json"
  "examples/crowds/hy-acurate/dragon.json"
  "examples/crowds/hy-acurate/illinois.json"
  "examples/crowds/hy-acurate/moesi.json"
)

LOGDIR=log

mkdir -p "${LOGDIR}"

for file in ${EXPERIMENTS[@]}
do
  echo "processing ${file}..."
  python3 main.py -qqqq --log="${LOGDIR}/$(basename -- "${file}" .json).log.json" ${file}
done
