#!python3
from parser import parse_file

import argparse
import logging
import analysis
import sys
import repl
import datetime as dt
from pathlib import Path

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("file",
                        help="file to be processed")

    parser.add_argument("--log",
                        help="prints log in JSON to provided file",
                        action="store")

    parser.add_argument("--interactive",
                        help=("allows user to disprove counter examples "
                              "interactively"),
                        action="store_true")

    parser.add_argument("-v",
                        help=("increases debug output level"
                              + " " + "(can be specified more than once)"),
                        action="count",
                        default=0)

    parser.add_argument("-q",
                        help=("decreases debug output level"
                              + " " + "(can be specified more than once)"),
                        action="count",
                        default=0)

    args = parser.parse_args()

    verbosity = 2 + args.v - args.q
    verbosity = max(0, min(verbosity, 4))

    if verbosity == 0:
        logging.basicConfig(level=logging.CRITICAL)
    elif verbosity == 1:
        logging.basicConfig(level=logging.ERROR)
    elif verbosity == 2:
        logging.basicConfig(level=logging.WARNING)
    elif verbosity == 3:
        logging.basicConfig(level=logging.INFO)
    elif verbosity == 4:
        logging.basicConfig(level=logging.DEBUG)


    path = Path(str(args.file))
    try:
        init_beginning = dt.datetime.now()
        ana = analysis.Analysis(path)
        init_end = dt.datetime.now()
        init_time = (init_end - init_beginning) / dt.timedelta(milliseconds=1)
        result_logs = {"initialization time": f"{init_time}ms"}
        for prop in ("deadlock",) + ana.system.property_names:
            if prop in ana.system.skip:
                continue
            if args.interactive:
                alg = repl.InteractiveGeneralizer(ana, prop)
            else:
                alg = analysis.Generalization(ana, prop)

            check_beginning = dt.datetime.now()
            alg.check()
            check_end = dt.datetime.now()
            check_time = ((check_end - check_beginning)
                          / dt.timedelta(milliseconds=1))

            minimize_time = None
            if alg.result:
                minimize_beginning = dt.datetime.now()
                alg.remove_unused_invariants()
                minimize_end = dt.datetime.now()
                minimize_time = ((minimize_end - minimize_beginning)
                                 / dt.timedelta(milliseconds=1))

            if verbosity > 0:
                if alg.result:
                    print(f"Successfully established property {prop}. "
                          f"({check_time}ms)")
                else:
                    print(f"Could not establish property {prop}. "
                          f"({check_time}ms)")
            if args.log:
                result_logs[prop] = alg.jsonify()
                result_logs[prop]["time"] = f"{check_time}ms"
                result_logs[prop]["reduction time"] = (f"{minimize_time}ms"
                                                       if minimize_time
                                                       else None)
        if args.log:
            import json
            with open(args.log, "w") as f:
                print(json.dumps(result_logs, indent=4), file=f)
    except Exception as e:
        print(f"Something went wrong for file {path}: {e}")
        if verbosity == 4:
            raise e
        sys.exit(2)


if __name__ == "__main__":
    main()
