#!/bin/env bash
podman run --rm --env HOST_OS=Linux -v $(pwd):/ostrich -w /ostrich -it ostrich:latest "$@"
