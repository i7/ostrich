import dataclasses
import typing
import logging
import mona

logger = logging.getLogger(__name__)

@dataclasses.dataclass(frozen=True)
class Term:
    @property
    def variables(self) -> typing.FrozenSet["Variable"]:
        raise NotImplementedError()

    @property
    def constants(self) -> typing.FrozenSet["Constant"]:
        raise NotImplementedError()

    def rename(self, renaming: typing.Mapping["Term", "Term"]) -> "Term":
        return renaming.get(self, self)

    def render(self) -> str:
        raise NotImplementedError()


@dataclasses.dataclass(frozen=True)
class Variable(Term):
    name: str

    @property
    def variables(self) -> typing.FrozenSet["Variable"]:
        return frozenset({self})

    @property
    def constants(self) -> typing.FrozenSet["Constant"]:
        return frozenset()

    def render(self) -> str:
        return self.name


@dataclasses.dataclass(frozen=True)
class Constant(Term):
    value: int

    @property
    def variables(self) -> typing.FrozenSet["Variable"]:
        return frozenset()

    @property
    def constants(self) -> typing.FrozenSet["Constant"]:
        return frozenset({self})

    def render(self) -> str:
        return str(self.value)


@dataclasses.dataclass(frozen=True)
class Formula:
    @property
    def variables(self) -> typing.FrozenSet[Variable]:
        raise NotImplementedError()

    @property
    def constants(self) -> typing.FrozenSet[Constant]:
        raise NotImplementedError()

    def rename(self, renaming: typing.Mapping[Term, Term]) -> "Formula":
        raise NotImplementedError()

    def render(self) -> str:
        raise NotImplementedError()


@dataclasses.dataclass(frozen=True)
class Truth(Formula):
    @property
    def variables(self) -> typing.FrozenSet[Variable]:
        return frozenset()

    @property
    def constants(self) -> typing.FrozenSet[Constant]:
        return frozenset()

    def rename(self, renaming: typing.Mapping[Term, Term]) -> "Truth":
        return self

    def render(self) -> str:
        return "true"


@dataclasses.dataclass(frozen=True)
class Negation:
    inner: Formula

    @property
    def variables(self) -> typing.FrozenSet[Variable]:
        return self.inner.variables

    @property
    def constants(self) -> typing.FrozenSet[Constant]:
        return self.inner.constants

    def rename(self, renaming: typing.Mapping[Term, Term]) -> "Negation":
        return type(self)(self.inner.rename(renaming))

    def render(self) -> str:
        return f"~({self.inner.render()})"


@dataclasses.dataclass(frozen=True)
class Junction(Formula):
    left: Formula
    right: Formula

    @property
    def variables(self) -> typing.FrozenSet[Variable]:
        return frozenset(self.left.variables | self.right.variables)

    @property
    def constants(self) -> typing.FrozenSet[Constant]:
        return frozenset(self.left.constants | self.right.constants)

    def rename(self, renaming: typing.Mapping[Term, Term]) -> "Junction":
        return type(self)(self.left.rename(renaming),
                          self.right.rename(renaming))


@dataclasses.dataclass(frozen=True)
class Conjunction(Junction):
    def render(self) -> str:
        return f"({self.left.render()}) & ({self.right.render()})"


@dataclasses.dataclass(frozen=True)
class Disjunction(Junction):
    def render(self) -> str:
        return f"({self.left.render()}) | ({self.right.render()})"


@dataclasses.dataclass(frozen=True)
class Relation(Formula):
    left: Term
    right: Term

    @property
    def variables(self) -> typing.FrozenSet[Variable]:
        return frozenset(self.left.variables | self.right.variables)

    @property
    def constants(self) -> typing.FrozenSet[Constant]:
        return frozenset(self.left.constants | self.right.constants)

    def rename(self, renaming: typing.Mapping[Term, Term]) -> "Relation":
        return type(self)(self.left.rename(renaming),
                          self.right.rename(renaming))


@dataclasses.dataclass(frozen=True)
class Equality(Relation):
    def render(self) -> str:
        return f"{self.left.render()} = {self.right.render()}"


@dataclasses.dataclass(frozen=True)
class Smaller(Relation):
    def render(self) -> str:
        return f"{self.left.render()} < {self.right.render()}"


@dataclasses.dataclass(frozen=True)
class IsNext(Relation):
    def render(self) -> str:
        return f"is_next({self.left.render()}, {self.right.render()})"
