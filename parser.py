import lark  # type: ignore
import logging
import formula
import json
import system
import typing
import pathlib


logger = logging.getLogger(__name__)


grammar_file = "formula.lark"
logger.debug(f"Instantiating parser from {grammar_file}")
parser = lark.Lark(open(grammar_file), start="formula")

def parse_rendezvous(json_dict) -> typing.Tuple[typing.Dict[formula.Term,
                                                            formula.Term],
                                                system.Rendezvous]:
    try:
        guard = parse_formula(json_dict["guard"])
        del(json_dict["guard"])
    except KeyError:
        guard = formula.Truth()
    variables: typing.List[formula.Variable] = list(guard.variables)
    constants: typing.List[formula.Constant] = list(guard.constants)
    changes: typing.Dict[formula.Term, typing.List[system.StateChange]] = {}
    for key in json_dict:
        try:
            term: formula.Term = formula.Constant(int(key))
            constants.append(term)  # type: ignore
        except ValueError:
            term = formula.Variable(str(key))
            variables.append(term)
        changes[term] = [
                system.StateChange(
                    tuple(curr["preset"]),
                    tuple(curr["postset"]))
                for curr in json_dict[key]]
    # Get all variables mentioned in the guard and for which we have changes:
    variables = sorted(set(variables),
                                                      key=lambda s: s.name)
    variable_renaming = dict(zip(variables,
                                 system.rendezvous_variables(len(variables))))
    return (variable_renaming,  # type: ignore
            system.Rendezvous(
                guard.rename(variable_renaming),
                tuple([(variable_renaming[var], tuple(changes[var]))  # type: ignore
                       for var in variables if var in changes]
                      + [(const, tuple(changes[const]))  # type: ignore
                         for const in constants if const in changes])))

def parse_broadcast(json_dict, variable_renaming):
    guard = (parse_formula(json_dict["guard"]) if "guard" in json_dict
                                               else formula.Truth())
    broadcast_variables = guard.variables - set(variable_renaming.keys())
    if len(broadcast_variables) == 0:
        broadcast_variable = system.broadcast_variable
    elif len(broadcast_variables) == 1:
        broadcast_variable = next(iter(broadcast_variables))
    else:
        raise ValueError(f"Found too many broadcast variables: "
                         f"{broadcast_variables}.")
    broadcast_renaming = {**variable_renaming,
                          broadcast_variable: system.broadcast_variable}
    changes = json_dict["changes"]
    return system.Broadcast(
            guard.rename(broadcast_renaming),
            tuple([system.StateChange(
                tuple(v["preset"]),
                tuple(v["postset"]))
                for v in changes]))

def parse_transition(json_dict):
    if "rendezvous" in json_dict:
        renaming, rendezvous = parse_rendezvous(json_dict["rendezvous"])
    else:
        renaming = {}
        rendezvous = system.Rendezvous(formula.Truth(), tuple())

    broadcasts = ([parse_broadcast(v, renaming)
                   for v in json_dict["broadcasts"]]
                  if "broadcasts" in json_dict
                  else [])
    return system.Transition(
            rendezvous,
            tuple(broadcasts))

def parse_initial(json_dict):
    if not "others" in json_dict:
        raise ValueError("Cannot find default initial states.")
    default_initial_states = tuple(sorted(json_dict["others"]))
    del json_dict["others"]
    special_initial_states = {}
    for key in json_dict:
        try:
            constant = formula.Constant(int(key))
        except ValueError as e:
            raise ValueError(
                    f"Failed to parse initial states for constant: {key}")
        special_initial_states[constant] = tuple(sorted(json_dict[key]))
    return (default_initial_states, tuple(special_initial_states.items()))


def parse_topology(json_dict):
    try:
        value = json_dict["topology"]
        if value == "ring":
            return system.Topology.RING
        elif value == "headed ring":
            return system.Topology.HEADEDRING
        elif value == "crowd":
            return system.Topology.CROWD
        elif value == "array":
            return system.Topology.ARRAY
        else:
            raise ValueError(f"Specification defines unkown topology <{value}>")
    except KeyError:
        return None


def parse_spec(json_dict):
    states = sorted(json_dict["states"])
    cover = [sorted(cover_elem) for cover_elem in json_dict["cover"]]
    default_initial_states, special_initial_states = parse_initial(
            json_dict["initial"])
    transitions = map(parse_transition, json_dict["transitions"])
    properties = parse_properties(json_dict.get("properties", {}))
    skip = json_dict.get("skip", [])
    topology = parse_topology(json_dict)
    minimal_size = int(json_dict.get("minimal_size", 2))
    return system.System(tuple(states),
                         tuple(tuple(cover_elem) for cover_elem in cover),
                         default_initial_states,
                         special_initial_states,
                         topology,
                         tuple(transitions),
                         properties,
                         tuple(skip),
                         minimal_size)

def parse_properties(json_dict):
    return tuple({str(key): str(json_dict[key])
                  for key in json_dict}.items())

def parse_file(filepath: pathlib.Path):
    with open(filepath, mode="r") as f:
        system = parse_spec(json.loads(f.read()))
        return system


def parse_formula(text):
    return FormulaParser().transform(parser.parse(text))


class FormulaParser(lark.Transformer):
    @lark.v_args(inline=True)
    def constant(self, value):
        logger.debug(f"Parsing constant value {str(value)}")
        return formula.Constant(int(value))

    @lark.v_args(inline=True)
    def variable(self, name):
        logger.debug(f"Parsing variable term with name {str(name)}")
        return formula.Variable(str(name))

    @lark.v_args(inline=True)
    def negation(self, inner):
        logger.debug(f"Parsing negation of {inner}")
        return formula.Negation(inner)

    def conjunction(self, args):
        if len(args) == 1:
            return args[0]
        elif len(args) == 2:
            left, right = args
            logger.debug(f"Parsing conjunction of {left} and {right}")
            return formula.Conjunction(left, right)
        else:
            raise ValueError(f"Cannot parse conjunction of {args}")

    def disjunction(self, args):
        if len(args) == 1:
            return args[0]
        elif len(args) == 2:
            left, right = args
            logger.debug(f"Parsing disjunction of {left} and {right}")
            return formula.Disjunction(left, right)
        else:
            raise ValueError(f"Cannot parse disjunction of {args}")

    @lark.v_args(inline=True)
    def equality(self, left, right):
        logger.debug(f"Parsing equality of {left} and {right}")
        return formula.Equality(left, right)

    @lark.v_args(inline=True)
    def inequality(self, left, right):
        logger.debug(f"Parsing inequality of {left} and {right}")
        return formula.Negation(formula.Equality(left, right))

    @lark.v_args(inline=True)
    def smaller(self, left, right):
        logger.debug(f"Parsing {left} < {right}")
        return formula.Smaller(left, right)

    @lark.v_args(inline=True)
    def smaller_eq(self, left, right):
        logger.debug(f"Parsing {left} <= {right}")
        return formula.Disjunction(formula.Smaller(left, right),
                                   formula.Equality(left, right))

    @lark.v_args(inline=True)
    def larger(self, left, right):
        logger.debug(f"Parsing {left} > {right}")
        return formula.Smaller(right, left)

    @lark.v_args(inline=True)
    def larger_eq(self, left, right):
        logger.debug(f"Parsing {left} >= {right}")
        return formula.Disjunction(formula.Smaller(right, left),
                                   formula.Equality(left, right))

    @lark.v_args(inline=True)
    def is_next(self, left, right):
        logger.debug(f"Parsing {left} >> {right}")
        return formula.IsNext(left, right)

    @lark.v_args(inline=True)
    def is_prev(self, left, right):
        logger.debug(f"Parsing {left} << {right}")
        return formula.IsNext(right, left)

    @lark.v_args(inline=True)
    def true(self):
        return formula.Truth()


if __name__ == "__main__":
    import sys
    import json
    import logging
    logging.basicConfig(level=logging.INFO)
    with open(sys.argv[1], mode="r") as f:
        system = parse_spec(json.loads(f.read()))
        print(system)
