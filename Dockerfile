FROM ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y python3 python3-pip mona

RUN python3 -m pip install lark-parser
RUN python3 -m pip install jinja2
RUN python3 -m pip install dataclasses


RUN rm -rf /var/lib/apt/lists/*
