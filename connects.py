#!python3
from parser import parse_file

import argparse
import logging
import analysis
import sys
import repl
import datetime as dt
from pathlib import Path

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("file",
                        help="file to be processed")

    args = parser.parse_args()

    path = Path(str(args.file))
    try:
        ana = analysis.Analysis(path)
        print(ana.transition_formula())
    except Exception as e:
        print(f"Something went wrong for file {path}: {e}")
        sys.exit(2)


if __name__ == "__main__":
    main()
